# Github Dashboard

This repo contain development kit for development of the github dashboard. This 

# Component
 
Backend - The backend component is at `https://gitlab.com/bjoe88/github-dashboard-backend.git`. 

Frontend - The frontend component is at `https://gitlab.com/bjoe88/github-dashboard-frontend.git`

# Running for development

To run this application. Just run the sh script `init.sh`. This script will clone the 2 components needed. Hence you will need to have access to it. Also to run this application, the machine will need to have the latest angular-cli. To run this application properly, you will need to have atleast Github personal access token. Put the token at the `config.js` before running the `init.sh`
