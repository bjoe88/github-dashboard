#!/bin/bash
echo 'Check github-dashboard-frontend folder'
if [ ! -d "github-dashboard-frontend" ]; then
    mkdir github-dashboard-frontend
    cd github-dashboard-frontend
    echo 'Pull github-dashboard-frontend from Repo'
    git clone git@gitlab.com:bjoe88/github-dashboard-frontend.git .
    cd ..
else
    cd github-dashboard-frontend
    echo 'Pull latest change for github-dashboard-frontend'
    git pull
    cd ..
fi

echo 'Check github-dashboard-backend folder'
if [ ! -d "github-dashboard-backend" ]; then
    mkdir github-dashboard-backend
    cd github-dashboard-backend
    echo 'Pull github-dashboard-backend from Repo'
    git clone git@gitlab.com:bjoe88/github-dashboard-backend.git .
    cd ..
else
    cd github-dashboard-backend
    echo 'Pull latest change for github-dashboard-backend'
    git pull
    cd ..
fi
docker-compose up -d


echo -en "\rWaiting [.....]";
sleep 1s
echo -en "\rWaiting [-....]";
sleep 1s
echo -en "\rWaiting [--...]";
sleep 1s
echo -en "\rWaiting [---..]";
sleep 1s
echo -en "\rWaiting [----.]";
sleep 1s
echo -e "\rWaiting [-----]";

cd github-dashboard-frontend
npm i;
ng serve;